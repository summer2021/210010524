import json
import os

from flask import Flask
from flask import jsonify, request

import logging

import config
import redis_queue
from utils import *

logging.basicConfig(level=config.LOG_LEVEL,
                    format='%(asctime)s|%(levelname)s|%(filename)s[%(lineno)d]|%(message)s')

app = Flask("iSUla Schedule")

q = redis_queue.RedisQueue("common")


# TODO: authentication
@app.get("/")
def main():
    logging.debug("hello")
    return jsonify({"hello": "world"})


# Maybe workspace_id is better?
# Maybe put is better
@app.get("/create/<string:workspace_name>")
def create(workspace_name):
    logging.info("Ready to create workspace [%s]." % workspace_name)

    input_path = "%s/%s" % (config.INPUT_DIR, workspace_name)
    output_path = "%s/%s" % (config.OUTPUT_DIR, workspace_name)

    if os.path.exists(input_path) or os.path.exists(output_path):
        logging.debug("workspace [%s] has exist." % workspace_name)
        return jsonify(json_message(FAILURE, error_messages["WORKSPACE_EXIST"]))

    try:
        os.makedirs(input_path)
        os.makedirs(output_path)
    except NotADirectoryError:
        logging.debug("workspace directory create failed.")
        return jsonify(json_message(FAILURE, error_messages["DIR_CREATE_FAIL"]))
    except Exception:
        logging.debug("exception.")
        return jsonify(json_message(FAILURE, error_messages["OTHER"]))

    logging.info("workspace [%s] created successfully." % workspace_name)
    return jsonify(json_message(SUCCESS, "create successfully"))


@app.post("/upload/<string:workspace_name>")
def upload(workspace_name):
    logging.info("Ready to receive file to workspace [%s]." % workspace_name)

    path = "%s/%s" % (config.INPUT_DIR, workspace_name)

    if "file" not in request.files:
        logging.debug("post request does not have the file part.")
        return jsonify(json_message(FAILURE, error_messages["NO_FILE"]))

    file = request.files['file']

    if file.filename == '':
        logging.debug("filename is empty.")
        return jsonify(json_message(FAILURE, error_messages["EMPTY_FILENAME"]))

    file.save(os.path.join(path, file.filename))
    logging.info("receive [%s] to [%s]" % (file.filename, workspace_name))

    return jsonify(json_message(SUCCESS, "upload successfully."))


"""
{
    "image": "ubuntu",
    "image_version": "1.0",
    "files": [],
    "output_file": "filename",
    "timeout": 0,
    "command": "sh script.sh",
    "callback_url": "..."
}
"""


@app.post("/execute/<string:workspace_name>")
def execute(workspace_name):
    logging.info("execute workspace [%s]." % workspace_name)
    job = request.json

    job["name"] = workspace_name

    if job["emergency"] is True:
        q.put_emergency(json.dumps(job))
    else:
        q.put(json.dumps(job))

    return jsonify(json_message(SUCCESS, "success"))


@app.get("/output/<string:workspace_name>/<string:output_file>")
def output(workspace_name, output_file):
    # set header
    logging.info("get workspace [%s] result file [%s]." % (workspace_name, output_file))

    output_path = "%s/%s/%s" % (config.OUTPUT_DIR, workspace_name, output_file)

    if not os.path.exists(output_path):
        logging.debug("workspace [%s/%s] has exist." % (workspace_name, output_file))
        return jsonify(json_message(FAILURE, error_messages["OUTPUT_NOT_EXIST"]))

    with open(output_path, "r", encoding="utf8") as f:
        output_dict = {"output": f.read()}

    return jsonify(json_message(SUCCESS, "success", output_dict))


if __name__ == '__main__':
    app.run()
