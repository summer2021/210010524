import multiprocessing
import subprocess
import re
import sys
import time
import os
import shutil
from multiprocessing.connection import Connection
import requests

import config
import select
import json

import isulapy.isula as isula

q = []
bind = {}


def timeout_with_return_status(command, time_out, return_value=0):
    """
    run $ timeout time_out command > /dev/null 2>&1
    until time_out or ret_code == returnValue

    timeoutWithReturnStatus - Run a Unix command with a timeout,
    until the expected value is returned by the command; On timeout,
    return last error code obtained from the command.
    """

    # $ command > /dev/null 2>&1
    p = subprocess.Popen(
        command, stdout=open("/dev/null", "w"), stderr=subprocess.STDOUT
    )
    t = 0.0
    ret = -1
    while t < time_out:
        ret = p.poll()
        if ret is None:
            time.sleep(config.TIMER_POLL_INTERVAL)
            t += config.TIMER_POLL_INTERVAL
        elif ret == return_value:
            return ret
        else:
            p = subprocess.Popen(
                command, stdout=open("/dev/null", "w"), stderr=subprocess.STDOUT
            )
    return ret


class LocalIsulaCli(object):
    def __init__(self):
        if len(config.ISULA_VOLUME_PATH) == 0:
            exit(1)

    def instance_name(self, job_name):
        return "%s-%s" % (config.PREFIX, job_name)

    def get_volume_path(self, instance_name):
        return os.path.join(config.ISULA_VOLUME_PATH, instance_name, "")

    def copy_in(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        # Create a fresh volume
        os.makedirs(volume_path, exist_ok=True)
        for file in job["files"]:
            # Create output directory if it does not exist
            os.makedirs(os.path.dirname(volume_path), exist_ok=True)

            shutil.copy(config.INPUT_DIR + "/" + job["name"] + "/" + file, volume_path + file)
        return 0

    def run_job(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        args = ["isula", "run", "--net", "host", "--annotation", "native.umask=normal", "--name", instance_name, "-v"]
        args = args + ["%s:%s" % (volume_path, "/home/mount")]
        args = args + ["{}:{}".format(job["image"], job["image_version"])]
        args = args + ["sh", "-c"]

        args = args + ["cd /home/mount; %s" % job["command"]]

        pipes = multiprocessing.Pipe()

        print(args)

        p = subprocess.Popen(
            args, stdout=open("/dev/null", "w"), stderr=pipes[0].fileno()
        )

        current = time.time()

        if job["timeout"] == 0:
            q.append((9223372036.854775, pipes[1]))
        else:
            q.append((current + job["timeout"], pipes[1]))

        q.sort()

        bind[pipes[1]] = {
            "p": p,
            "job": job,
            "begin_time": current,
            "due": current + job["timeout"]
        }

    def copy_out(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        try:
            shutil.move(volume_path + job["output_file"],
                        config.OUTPUT_DIR + '/' + job["name"] + "/" + job["output_file"])
            if job["callback_url"] != "":
                requests.post(job["callback_url"])
        except FileNotFoundError:
            print("no output file")

        self.destroy_vm(job)

        return 0

    def destroy_vm(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        timeout_with_return_status(["isula", "rm", "-f", instance_name], config.ISULA_RM_TIMEOUT)

        if instance_name in os.listdir(volume_path):
            shutil.rmtree(volume_path + instance_name)

    def get_images(self):
        """getImages - Executes `isula images` and returns a list of
        images that can be used to boot a isula container with. This
        function is a lot of parsing and so can break easily.
        """
        result = set()
        o = subprocess.check_output("isula images", shell=True).decode("utf-8")
        o_l = o.split("\n")
        o_l.pop()
        o_l.reverse()
        o_l.pop()
        for row in o_l:
            row_l = row.split(" ")
            result.add(re.sub(r".*/([^/]*)", r"\1", row_l[0]))
        return list(result)


class LocalIsulaCri(object):
    def __init__(self):
        if len(config.ISULA_VOLUME_PATH) == 0:
            exit(1)

    def instance_name(self, job_name):
        return "%s-%s" % (config.PREFIX, job_name)

    def get_volume_path(self, instance_name):
        return os.path.join(config.ISULA_VOLUME_PATH, instance_name, "")

    def copy_in(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        # Create a fresh volume
        os.makedirs(volume_path, exist_ok=True)
        for file in job["files"]:
            # Create output directory if it does not exist
            os.makedirs(os.path.dirname(volume_path), exist_ok=True)

            shutil.copy(config.INPUT_DIR + "/" + job["name"] + "/" + file, volume_path + file)
        return 0

    def run_job(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        # --net host
        container_config = {
            "image": "{}:{}".format(job["image"], job["image_version"]),
            "cmd": "sh -c cd /home/mount; %s" % job["command"],
            "container_path": "/home/mount",
            "host_path": volume_path
        }

        return isula.run_container(instance_name, container_config)

    def copy_out(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        shutil.move(volume_path + job["output_file"], config.OUTPUT_DIR + '/' + job["name"] + "/" + job["output_file"])
        self.destroy_vm(job)

        return 0

    def destroy_vm(self, job):
        instance_name = self.instance_name(job["name"])
        volume_path = self.get_volume_path(instance_name)

        isula.remove_container(instance_name)

        if instance_name in os.listdir(volume_path):
            shutil.rmtree(volume_path + instance_name)

    def get_images(self):
        """getImages - Executes `isula images` and returns a list of
        images that can be used to boot a isula container with. This
        function is a lot of parsing and so can break easily.
        """
        result = set()
        o = subprocess.check_output("isula images", shell=True).decode("utf-8")
        o_l = o.split("\n")
        o_l.pop()
        o_l.reverse()
        o_l.pop()
        for row in o_l:
            row_l = row.split(" ")
            result.add(re.sub(r".*/([^/]*)", r"\1", row_l[0]))
        return list(result)


vmms = LocalIsulaCli()


def event_loop(pipe: Connection):
    q.append((9223372036.854775, pipe))
    q.sort()
    while True:
        readable, _, _ = select.select([item[1] for item in q], [], [], q[0][0])
        for p in readable:
            if p is pipe:
                job = json.loads(pipe.recv())
                vmms.copy_in(job)
                vmms.run_job(job)

            else:
                value = bind[p]
                if time.time() >= value["due"]:
                    os.kill(value["p"].pid, 9)
                    continue
                vmms.copy_out(value["job"])
                del bind[p]
                for item in q:
                    if item[1] is p:
                        q.remove(item)
                        break
