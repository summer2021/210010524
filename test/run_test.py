import vmms_isula

job = {
    "name": "test1",
    "image": "ubuntu",
    "image_version": "latest",
    "files": ["t.sh"],
    "output_file": "out.txt",
    "timeout": 100,
    "command": "sh t.sh > out.txt",
    "callback_url": "..."
}

li = vmms_isula.LocalIsulaCli()

li.copy_in(job)
li.run_job(job)
li.copy_out(job)
