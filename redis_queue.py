import redis

import config

redis_connection = None


def connect_redis():
    global redis_connection
    if redis_connection is None:
        redis_connection = redis.StrictRedis(host=config.redis_host, port=config.redis_port, db=0)

    return redis_connection


class RedisQueue:
    def __init__(self, queue_name):
        self.queue_name = queue_name
        self.__db = connect_redis()

    def qsize(self):
        return self.__db.llen(self.queue_name)

    def put(self, obj):
        self.__db.rpush(self.queue_name, obj)

    def put_emergency(self, obj):
        self.__db.lpush(self.queue_name, obj)

    def get(self, timeout=0, blocked=True):
        if blocked is True:
            return self.__db.blpop(self.queue_name, timeout=timeout)
        return self.__db.lpop(self.queue_name)
