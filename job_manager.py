import redis_queue
import multiprocessing

import vmms_isula

q = redis_queue.RedisQueue("common")

pipes = multiprocessing.Pipe()


def add_job(pipe):
    while True:
        job = q.get()[1]
        print(job)
        pipe.send(job)


if __name__ == '__main__':
    vmms2 = multiprocessing.Process(target=add_job(pipes[0]))
    vmms1 = multiprocessing.Process(target=vmms_isula.event_loop(pipes[1]))

    vmms1.start()
    vmms2.start()
