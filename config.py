import logging

INPUT_DIR = "job_files"
OUTPUT_DIR = "results"

LOG_LEVEL = logging.DEBUG

redis_host = "localhost"
redis_port = 6379

ISULA_VOLUME_PATH = "/home/zck1022/git/210010524/volumes"
ISULA_RM_TIMEOUT = 10

TIMER_POLL_INTERVAL = 1

PREFIX = "schedule_test"
