SUCCESS = 0
FAILURE = 1

error_messages = {
    "DIR_CREATE_FAIL": "directory create failed, please check FILE_DIR in config.py",
    "WORKSPACE_EXIST": "job has existed.",
    "NO_FILE": "No file in request, please check post request",
    "EMPTY_FILENAME": "filename is empty, please check the request",
    "OUTPUT_NOT_EXIST": "output file not exist",
    "OTHER": "other failure, please check log",
}


def json_message(status: int, message: str, other=None) -> dict:
    if other is None:
        other = {}
    return {"status": status, "message": message, **other}
